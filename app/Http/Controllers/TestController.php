<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Arr;

use PDF;
use Carbon\Carbon;

class TestController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        ini_set('memory_limit','512M');
        // variable
        $spbNo = '0';
        $spbFinals = array();

        // get SPB
        $spbs = \DB::select("select TOP 10 * From Vspb a WHERE a.ManifestId =  '10263'");

        // get SPB goods
        foreach ($spbs as $spb){ $spbNo = $spbNo . ',' . $spb->SpbId; }
//        $spbGoods = \DB::select('EXEC [SPManifestDetail] @CompanyId = 1,@ManifestId = 10263');
        $spbGoods = \DB::select('SELECT  * FROM VSpbGoods WHERE SpbId IN ('.$spbNo.')');



        // combine data
        foreach ($spbs as $spb){
            $carbon_date = Carbon::parse($spb->CreatedAt);
//            $carbon_date->addHours(7)->addMinutes(0)->format('Y-m-d');
            $carbon_date->addHours(7)->addMinutes(0);
            $spb->CreatedAtLocal = $carbon_date->format('d M Y H:i:s');
            $tmpGoods = array();
            foreach ($spbGoods as $spbGood) {
                if ($spbGood->SpbId == $spb->SpbId) { $tmpGoods[] = $spbGood;  }
           }
            $spb->goods = $tmpGoods;
            $spbCabang = clone($spb);


            // untuk Pelanggan
            $spb->PaymentMethodText = ($spb->PaymentMethod=='TT') ? '1 - Asli Pelanggan' : '2B - Salinan Penerima';
            $spbFinals[]=$spb;

            // untuk cabang

            $spbCabang->PaymentMethodText = '7 - Cabang';
            $spbFinals[]=$spbCabang;

            // membuat gambar QR CODE
            $image = \QrCode::format('png')
                ->size(200)->margin(0)
                ->generate($spb->SpbId);
            $output_file = 'qrcode-'.$spb->SpbId.'.png';
            \Storage::disk('local')->put($output_file, $image);


        }

//        print_r($spbFinals);




        $data = [
            'spbs' => $spbFinals,
            'sheets' => ['Pelanggan','Cabang'],
        ];


        $pdf = PDF::loadView('test', $data);
//        $pdf->setPaper('B5', 'landscape');

        return $pdf->stream('customers.pdf');
//        return view('test', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
