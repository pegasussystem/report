<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use PDF;

class SpbCreateController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, $spbNo, $workTimeZoneHour, $workTimeZoneMinute, $username)
    {
        // variable
        $utcDate = Carbon::now();
        $localDate = $utcDate->addHours($workTimeZoneHour)->addMinutes($workTimeZoneMinute)->format('d M Y H:i:s');

        $spbFinals = array();


        // get SPB
        $spbs = \DB::select("select * From Vspb a WHERE a.SpbNo =  '$spbNo'");


        // get SPB goods
//        $spbGoods = \DB::select('EXEC [SPManifestDetail] @CompanyId = 1,@ManifestId = 10263');
        $spbGoods = \DB::select('SELECT  * FROM VSpbGoods WHERE SpbId IN ('.$spbs[0]->SpbId.')');



        // combine data
        foreach ($spbs as $spb){
            $carbon_date = Carbon::parse($spb->CreatedAt);
//            $carbon_date->addHours(7)->addMinutes(0)->format('Y-m-d');
            $carbon_date->addHours($workTimeZoneHour)->addMinutes($workTimeZoneMinute);
            $spb->CreatedAtLocal = $carbon_date->format('d M Y H:i:s');
            $tmpGoods = array();
            foreach ($spbGoods as $spbGood) {
                if ($spbGood->SpbId == $spb->SpbId) { $tmpGoods[] = $spbGood;  }
            }
            $spb->goods = $tmpGoods;
            $spbGerai = clone($spb);


            // untuk pengantar
            $spb->PaymentMethodText = ($spb->PaymentMethod=='TA') ? '1 - Asli Pelanggan' : '2A - Salinan Pengirim';
            $spbFinals[]=$spb;

            // untuk gerai
            $spbGerai->PaymentMethodText = '3 - Gerai';
            $spbFinals[]=$spbGerai;


            // membuat gambar QR CODE
            $image = \QrCode::format('png')
                ->size(200)->margin(0)->errorCorrection('H')
                ->generate($spb->SpbNo);
            $output_file = 'qrcode-'.$spb->SpbId.'.png';
            \Storage::disk('local')->put($output_file, $image);

        }

        $data = [
            'spbs' => $spbFinals,
            'username' => $username,
            'localDate' => $localDate,
        ];


        $pdf = PDF::loadView('print-spb', $data);

        return $pdf->stream('SPB '.$spbNo.'.pdf');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function thermal(Request $request, $spbNo, $workTimeZoneHour, $workTimeZoneMinute, $username)
    {
        // variable
        $utcDate = Carbon::now();
        $localDate = $utcDate->addHours($workTimeZoneHour)->addMinutes($workTimeZoneMinute)->format('d M Y H:i:s');

        $spbFinals = array();


        // get SPB
        $spbs = \DB::select("select * From Vspb a WHERE a.SpbNo =  '$spbNo'");


        // get SPB goods
//        $spbGoods = \DB::select('EXEC [SPManifestDetail] @CompanyId = 1,@ManifestId = 10263');
        $spbGoods = \DB::select('SELECT  * FROM VSpbGoods WHERE SpbId IN ('.$spbs[0]->SpbId.')');



        // combine data
        foreach ($spbs as $spb){
            $carbon_date = Carbon::parse($spb->CreatedAt);
//            $carbon_date->addHours(7)->addMinutes(0)->format('Y-m-d');
            $carbon_date->addHours($workTimeZoneHour)->addMinutes($workTimeZoneMinute);
            $spb->CreatedAtLocal = $carbon_date->format('d M Y H:i:s');
            $tmpGoods = array();
            foreach ($spbGoods as $spbGood) {
                if ($spbGood->SpbId == $spb->SpbId) { $tmpGoods[] = $spbGood;  }
            }
            $spb->goods = $tmpGoods;
            $spbGerai = clone($spb);


            // untuk pengantar
            $spb->PaymentMethodText = ($spb->PaymentMethod=='TA') ? '1 - Asli Pelanggan' : '2A - Salinan Pengirim';
            $spbFinals[]=$spb;

            // untuk gerai
            $spbGerai->PaymentMethodText = '3 - Gerai';
            $spbFinals[]=$spbGerai;


            // membuat gambar QR CODE
            $image = \QrCode::format('png')
                ->size(200)->margin(0)->errorCorrection('H')
                ->generate($spb->SpbNo);
            $output_file = 'qrcode-'.$spb->SpbId.'.png';
            \Storage::disk('local')->put($output_file, $image);

        }

        $data = [
            'spbs' => $spbFinals,
            'username' => $username,
            'localDate' => $localDate,
        ];


        $pdf = PDF::loadView('print-spb-thermal', $data);
        $pdf->setPaper([0, 0, 226, 822]);

        return $pdf->stream('SPB '.$spbNo.'.pdf');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
