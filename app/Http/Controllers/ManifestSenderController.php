<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;


use PDF;

class ManifestSenderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, $manifestId, $workTimeZoneHour, $workTimeZoneMinute, $username)
    {
        ini_set('max_execution_time', '600'); //300 seconds = 5 minutes
        ini_set('memory_limit','512M');


        // variable
        $utcDate = Carbon::now();
        $localDate = $utcDate->addHours($workTimeZoneHour)->addMinutes($workTimeZoneMinute)->format('d M Y H:i:s');
        $spbNo = '0';
        $spbFinals = array();


        // get SPB
        $spbs = \DB::select("select * From Vspb a WHERE a.ManifestId =  '$manifestId'");

        // get SPB goods
        foreach ($spbs as $spb){ $spbNo = $spbNo . ',' . $spb->SpbId; }
//        $spbGoods = \DB::select('EXEC [SPManifestDetail] @CompanyId = 1,@ManifestId = 10263');
        $spbGoods = \DB::select('SELECT  * FROM VSpbGoods WHERE SpbId IN ('.$spbNo.')');



        // combine data
        foreach ($spbs as $spb){
            $carbon_date = Carbon::parse($spb->CreatedAt);
//            $carbon_date->addHours(7)->addMinutes(0)->format('Y-m-d');
            $carbon_date->addHours($workTimeZoneHour)->addMinutes($workTimeZoneMinute);
            $spb->CreatedAtLocal = $carbon_date->format('d M Y H:i:s');
            $tmpGoods = array();
            foreach ($spbGoods as $spbGood) {
                if ($spbGood->SpbId == $spb->SpbId) { $tmpGoods[] = $spbGood;  }
            }
            $spb->goods = $tmpGoods;
            $spbCabang = clone($spb);


            // untuk OPS
            $spb->PaymentMethodText = '';
            $spbFinals[]=$spb;


            // membuat gambar QR CODE
            $image = \QrCode::format('png')
                ->size(200)->margin(0)->errorCorrection('H')
                ->generate($spb->SpbNo);
            $output_file = 'qrcode-'.$spb->SpbId.'.png';
            \Storage::disk('local')->put($output_file, $image);

        }

        $data = [
            'spbs' => $spbFinals,
            'username' => $username,
            'localDate' => $localDate,
        ];


        $pdf = PDF::loadView('print-spb', $data);

        return $pdf->stream('List SPB dari Manifest '.$manifestId.'.pdf');
//        return view('manifest-receiver', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
