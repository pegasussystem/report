<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;


use PDF;

class SpbSenderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, $spbId, $workTimeZoneHour, $workTimeZoneMinute, $username)
    {
        //
        // variable
        $utcDate = Carbon::now();
        $localDate = $utcDate->addHours($workTimeZoneHour)->addMinutes($workTimeZoneMinute)->format('d M Y H:i:s');
        $spb= null;
        $spbGoods= null;
        $createdAtLocal = null;

        //
        // set variable

        //
        // logic

        // get spb data
        $spb = \DB::select("select * From Vspb a WHERE a.SpbId = $spbId");
        $createdAtLocal = Carbon::parse($spb[0]->CreatedAt);
        $createdAtLocal->addHours($workTimeZoneHour)->addMinutes($workTimeZoneMinute);
        $spb[0]->CreatedAtLocal =  $createdAtLocal->format('d M Y H:i:s');
        $spb[0]->PaymentMethodText = '';


        // get spb goods data
        $spbGoods = \DB::select('SELECT  * FROM VSpbGoods WHERE SpbId IN ('.$spbId.')');
        $spb[0]->goods = $spbGoods;

        // membuat gambar QR CODE
        $image = \QrCode::format('png')
            ->size(200)->margin(0)->errorCorrection('H')
            ->generate($spb[0]->SpbNo);
        $output_file = 'qrcode-'.$spb[0]->SpbId.'.png';
        \Storage::disk('local')->put($output_file, $image);



        $data = [
            'spbs' => $spb,
            'username' => $username,
            'localDate' => $localDate,
        ];

        //
        // return
        $pdf = PDF::loadView('print-spb', $data);
        return $pdf->stream('SPB '.$spb[0]->SpbNo.'.pdf');

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
