<html>

<head>
    <style>

        @page {
            size: B5 landscape;
        }

        html, body {
            font-family: 'Nunito', sans-serif;
            height: 100vh;
            margin: 10px;
            font-size: 9pt;
        }

        .tableChild {
            border: 0px;
        }

        .page_break { page-break-before: always; }

    </style>
</head>

<body>
@foreach ($spbs as $spb)
    @foreach ($sheets as $sheet)
        <table border="1" cellpadding="0" cellspacing="0" width="100%">
            <tr>
                <td colspan="4" valign="top">{{--HEADER--}}
                    <table border="1" cellpadding="3" cellspacing="0" width="100%"  class="tableChild" >
                        <tr>
                            <td width="218px;">
{{--                                <div> <img width="80px" src="{{public_path('img\logo-pegasus.jpg')}}" />   <span style="font: bold; font-size: 25px;">PEGASUS</span></div>--}}
                                <div> <img width="80px" src="{{url('img\logo-pegasus.jpg')}}" />   <span style="font: bold; font-size: 25px;">PEGASUS</span></div>
                                <div>Jl. Kebon kacang I No. 32 A & B</div>
                                <div>Jakarta Pusat</div>
                                <div>Telp. (021) 2123 4726, (021) 3101 768</div>
                            </td>
                            <td align="center" valign="Top">
                                <div style="font-size: 18px; font-weight: bold">SURAT PENGIRIMAN BARANG</div>
                                <div>{{$sheet}}</div>
                                <div style="font-size: 16px;  font-family: "Calibri";">{{ $spb->SpbNo }}</div>

                            </td>
{{--                            <td valign="top" width="125px;"> <img width="125px" src="{{public_path('img\spb\qrcode\qrcode-'.$spb->SpbId.'.png')}}" /> </td>--}}
                            <td valign="top" width="125px;"> <img width="125px" src="{{url('img\spb\qrcode\qrcode-'.$spb->SpbId.'.png')}}" /> </td>
                        </tr>

                    </table>

                </td>
            </tr>

            <tr>
                <td width="30%" valign="top"> {{--SENDER--}}
                    <table border="1" cellpadding="3" cellspacing="0" width="100%"  class="tableChild" >
                        <tr>
                            <td colspan="2" align="center">Lokasi Asal</td>
                            <td colspan="2" align="center">Lokasi Tujuan</td>
                        </tr>
                        <tr>
                            <td align="center">{{$spb->OriginCityCode}}</td> <td align="center" >{{$spb->OriginAreaCode}}</td>
                            <td align="center">{{$spb->DestinationCityCode}}</td> <td align="center" >{{$spb->DestinationAreaCode}}</td>
                        </tr>
                        <tr>
                            <td colspan="4">
                                Pengirim : <br/><br/>
                                {{$spb->SenderName}}<br/>
                                {{$spb->SenderStore}}<br/>
                                {{$spb->SenderPlace}}<br/>
                                {{$spb->SenderAddress}}<br/>
                                {{$spb->SenderPhone}}
                            </td>
                        </tr>
                    </table>

                </td>
                <td colspan="3" valign="top"> {{--goods--}}
                    <table border="1" cellpadding="3" cellspacing="0" width="100%"  class="tableChild" >
                        <tr>
                            <td>#</td>
                            <td align="center">Jml. Koli</td>
                            <td align="center">No. Koli</td>
                            <td align="center">Berat</td>
                            <td align="center">Dimensi</td>
                            <td align="center">Berat Final</td>
                        </tr>
                        {{$i = 1}}
                        @foreach ($spb->goods as $goods)
                            <tr>
                                <td>{{$i}}</td>
                                <td>{{$goods->KoliNo}}</td>
                                <td>1</td>
                                <td>{{ number_format($goods->Aw,0)}}</td>
                                <td>{{number_format($goods->Length,0)}} X {{number_format($goods->Width,0)}} X {{number_format($goods->Height,0)}}</td>
                                <td>{{number_format($goods->Caw,0)}}</td>
                            </tr>
                            {{$i ++}}
                        @endforeach


                    </table>
                </td>
            </tr>

            <tr>
                <td valign="top"> {{--RECEIVER--}}
                    Penerima : <br/><br/>
                    {{$spb->ReceiverName}}<br/>
                    {{$spb->ReceiverStore}}<br/>
                    {{$spb->ReceiverPlace}}<br/>
                    {{$spb->ReceiverAddress}}<br/>
                    {{$spb->ReceiverPhone}} <br/>
                    Via : {{$spb->ViaName}}
                </td>
                <td colspan="2" valign="top" width="40%"> {{--MODA--}}
                    <table border="1" cellpadding="3" cellspacing="0" width="100%"  class="tableChild" >
                        <tr>
                            <td align="cener">Komoditi</td>
                            <td align="cener">Moda</td>
                            <td align="cener">Q.O.S</td>
                            <td align="cener">K.O.S</td>
                        </tr>
                        <tr>
                            <td >{{$spb->TypesOfGoodsName}}</td>
                            <td >{{$spb->CarrierName}}</td>
                            <td >{{$spb->QualityOfService}}</td>
                            <td >{{$spb->TypeOfService}}</td>
                        </tr>
                        <tr>
                            <td colspan="4" style="border: 0px;">
                                Keterangan : <br/> <br/>
                                {{$spb->Description}}
                            </td>
                        </tr>

                    </table>
                </td>
                <td valign="top"> {{--goods--}}
                    <table border="1" cellpadding="3" cellspacing="0" width="100%"  class="tableChild" >
                        <tr>
                            <td >Tarif</td> <td >{{number_format($spb->Rates,0)}}</td>
                        </tr>
                        <tr>
                            <td >Packing</td> <td >{{number_format($spb->Packing,0)}}</td>
                        </tr>
                        <tr>
                            <td >Karantina</td> <td >{{number_format($spb->Quarantine,0)}}</td>
                        </tr>
                        <tr>
                            <td >Lain - Lain </td> <td >{{number_format($spb->Etc,0)}}</td>
                        </tr>
                        <tr>
                            <td >PPN</td> <td >{{number_format($spb->Ppn,0)}}% ( {{ number_format($spb->TotalPrice*($spb->Ppn/100),0) }} ) </td>
                        </tr>
                        <tr>
                            <td >Disc.</td> <td >{{number_format($spb->Discount,0)}}% ( {{ number_format($spb->TotalPrice*($spb->Ppn/100),0) }} )</td>
                        </tr>
                        <tr>
                            <td >Total</td> <td >{{number_format($spb->TotalPrice,0)}}</td>
                        </tr>
                        <tr>
                            <td >Cara Bayar</td> <td >{{$spb->PaymentMethod}}</td>
                        </tr>


                    </table>

                </td>
            </tr>

            <tr>
                <td valign="top">
                    <div style="font-size: 7px;">
                        Pengirim menyataan bahwa keterangan yang ditulis pda halaman ini adalah benar dan barang yang dikirim tidak ada yang ilegal, zat berbahaya & narkoba. Untuk itu Pegasus tidak bertanggung jawab atas isi barang yang dikirim ini. Bahwa penggantian atas kerusakan/kehilangan barang disesuaikan dengan ketentuan angkutan dengan maksimum penggantian sebesar Rp.100.000,- (Seratur Ribu Rupiah) per Kg
                    </div>

                </td>
                <td valign="top" align="center">
                    Pengirim <br/><br/><br/><br/><br/>
                    Nama & Tanda Tangan
                </td>
                <td valign="top" align="center">
                    Petugas <br/>
                    {{$spb->Username}}<br/><br/><br/><br/>
                    DD MMM YYYY hh:mm:ss
                </td>
                <td valign="top" align="center">
                    Penerima<br/><br/><br/><br/><br/>
                    Nama, Tanda Tangan, Tanggal, Jam
                </td>
            </tr>


        </table>
        <div class="page_break"></div>
    @endforeach

@endforeach


</body>

</html>
