<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::resource('test', 'TestController');
Route::get('manifest-receiver/{manifestId}/{workTimeZoneHour}/{workTimeZoneMinute}/{username}', 'ManifestReceiverController@index');
Route::get('manifest-sender/{manifestId}/{workTimeZoneHour}/{workTimeZoneMinute}/{username}', 'ManifestSenderController@index');
Route::get('spb-sender/{spbId}/{workTimeZoneHour}/{workTimeZoneMinute}/{username}', 'SpbSenderController@index');
Route::get('spb-receiver/{spbId}/{workTimeZoneHour}/{workTimeZoneMinute}/{username}', 'SpbReceiverController@index');
Route::get('spb-create/{spbNo}/{workTimeZoneHour}/{workTimeZoneMinute}/{username}', 'SpbCreateController@index');
Route::get('spb-create-thermal/{spbNo}/{workTimeZoneHour}/{workTimeZoneMinute}/{username}', 'SpbCreateController@thermal');
